## Miply Hotjar

Magento 2 module that allows easy configuration of Hotjar. 

### Legal note
Hotjar is third-party name and trademark. All rights reserved. Trademark is for reference only.
It is not an official HotJar module, although build with good intentions to be compatible with Hotjar system and promote its usage by making it easier and scalable inside Magento. 