<?php
/**
 * Copyright © 2021 Miply. All rights reserved.
 *
 * See LICENSE.txt for license details.
 *
 * @copyright   Copyright (c) 2021 Miply
 * @author      Miply <magento@miply.no>
 */

namespace Miply\Hotjar\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Miply\Hotjar\Helper\Config;

class Head extends Template
{

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * Hotjar constructor.
     *
     * @param Config $config
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $keyInfo     =  parent::getCacheKeyInfo();

        $keyInfo[]   =  $this->isEnabled();
        $keyInfo[]   =  $this->getHotjarId();
        $keyInfo[]   =  $this->getHotjarSv();

        return $keyInfo;
    }

    /**
     * Check if the Hotjar module is enabled in admin
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isEnabled();
    }

    /**
     * Get Hotjar ID
     *
     * @return string
     */
    public function getHotjarId(): string
    {
        return $this->config->getId();
    }

    /**
     * Get Hotjar SV
     *
     * @return string
     */
    public function getHotjarSv(): string
    {
        return $this->config->getSv();
    }

    /**
     * Get Store Name
     *
     * @return string
     */
    public function getStoreName(): string
    {
        return (string) $this->storeManager->getStore()->getName();
    }
}