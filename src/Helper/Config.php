<?php
/**
 * Copyright © 2021 Miply. All rights reserved.
 *
 * See LICENSE.txt for license details.
 *
 * @copyright   Copyright (c) 2021 Miply
 * @author      Miply <magento@miply.no>
 */

namespace Miply\Hotjar\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Helper for module config
 */
class Config
{
    public const XPATH_MODULE = 'miply_hotjar';
    public const XPATH_CONFIG = 'configuration';
    public const XPATH_ENABLED = 'enabled';
    public const XPATH_HOTJAR_ID = 'hjid';
    public const XPATH_HOTJAR_SV = 'hjsv';

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (bool) $this->getConfigValue(self::XPATH_ENABLED);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return (string) $this->getConfigValue(self::XPATH_HOTJAR_ID);
    }

    /**
     * @return string
     */
    public function getSv(): string
    {
        return (string) $this->getConfigValue(self::XPATH_HOTJAR_SV);
    }

    /**
     * @param string $path
     * @param string $scopeType
     * @param string|null $scopeCode
     * @return mixed
     */
    private function getConfigValue(
        string $path,
        string $scopeType = ScopeInterface::SCOPE_STORE,
        string $scopeCode = null
    ) {
        return $this->getValue(self::XPATH_CONFIG . '/' . $path, $scopeType, $scopeCode);
    }

    /**
     * @param string $path
     * @param string $scopeType
     * @param string $scopeCode
     * @return mixed
     */
    private function getValue(
        string $path,
        string $scopeType = ScopeInterface::SCOPE_STORE,
        string $scopeCode = null
    ) {
        return $this->scopeConfig->getValue(
            self::XPATH_MODULE . '/' . $path,
            $scopeType,
            $scopeCode
        );
    }
}
