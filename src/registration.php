<?php
/**
 * Copyright © 2021 Miply. All rights reserved.
 *
 * See LICENSE.txt for license details.
 *
 * @copyright   Copyright (c) 2021 Miply
 * @author      Miply <magento@miply.no>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Miply_Hotjar',
    __DIR__
);
